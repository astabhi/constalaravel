<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\Config;


class ImageController extends Controller
{
    /*
     * client object for Guzzle
     */
    private $client;

    /*
     * Params to pass with API
     */
    private $params;

    public function __construct()
    {
        $this->client = new \GuzzleHttp\Client([
            'headers' => [
                'Content-type' => 'application/json',
                'Accept' => 'application/json',
                'x-api-key' => Config::get('custom.API_KEY')
            ]
        ]);

        $this->params = 'limit='.Config::get('custom.RECORDS_PER_PAGE').'&order=ASC';
    }

    public function getImages()
    {
        $request = $this->client->request('get', Config::get('custom.API_URL') . '/images/search?'.$this->params);
        $response = $request->getBody()->getContents();
//        dd($request->getHeaders());die;
        return \view('image.index',['data'=>json_decode($response)]);
    }

    public function imagesUploadAction(Request $request)
    {
        $client = new \GuzzleHttp\Client();
        $file = $request->file("image");
        echo "File Name: " . $file->getClientOriginalName() . "<br/>";
        echo "File Extension: " . $file->getClientOriginalExtension() . "<br/>";
        echo "File Real Path: " . $file->getRealPath() . "<br/>";
        echo "File Size: " . $file->getSize();
        $response = $client->request('POST', 'https://api.thecatapi.com/v1/images/upload', [
            'form_params' => [
                'image' => $file->getClientOriginalName(),
            ],
            'headers' => [
                'x-api-key' => \Config::get('custom.API_KEY')
            ]
        ]);
        $response = $response->getBody()->getContents();
        echo '<pre>';
        print_r($response);
    }
}

