<?php

return [

    /*
    |--------------------------------------------------------------------------
    | API URL
    |--------------------------------------------------------------------------
    |
    | Default host URL for API to use in applications various places
    */

    'API_URL' => env('API_URL', ''),


    /*
    |--------------------------------------------------------------------------
    | API KEY
    |--------------------------------------------------------------------------
    |
    | A secret key to use in headers along with API Calls,
    | Key is generated from API host after sign up
    */

    'API_KEY' => env('API_KEY', ''),


    /*
    |--------------------------------------------------------------------------
    | Record Per Page
    |--------------------------------------------------------------------------
    |
    | An integer will be responsible to display records pr result on the page
    | such as how many records we want to show on the page
    */
    'RECORDS_PER_PAGE' => env('RECORDS_PER_PAGE', ''),

];
