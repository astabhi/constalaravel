@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">Images</div>

                    <div class="panel-body">
                        @if(count($data))
                            @foreach($data as $image)
                                <div class="col-sm-6 col-md-4">
                                    <div class="thumbnail">
                                        <div class="img-thumbnail">
                                            <img src="{{$image->url}}" alt=""/>
                                        </div>
                                        <div class="caption">
                                            <p>
                                            <p>
                                            <div class="btn-group">
                                                <button class="btn btn-default btn-sm dropdown-toggle" type="button"
                                                        data-toggle="dropdown" aria-haspopup="true"
                                                        aria-expanded="false">
                                                    Select Rating <span class="caret"></span>
                                                </button>
                                                <ul class="dropdown-menu">
                                                    @for($i=1;$i<=10;$i++)
                                                        <li data-rating="{{$i}}" data-image="{{$image->id}}">{{$i}}</li>
                                                    @endfor
                                                </ul>
                                            </div>
                                            <a href="#" class="btn btn-primary" role="button">Vote</a>
                                            <a href="#" class="btn btn-default" role="button">Fav It</a></p>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
